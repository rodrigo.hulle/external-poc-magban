import Analytics from 'analytics';
import originalSrc from 'analytics-plugin-original-source';
import freshChatPlugin from './freshChat/index';
import hotjarPlugin from './hotjar/index';
import amplitudePlugin from './amplitude/index';

// Decalaração

const analytics = Analytics({
  app: 'magban-offer',
  debug: true,
  plugins: [
    originalSrc(),
    freshChatPlugin({
      whatEver: 'hello',
      elseYouNeed: 'there',
    }),
    hotjarPlugin({
      siteId: '1845659',
      debug: false,
      sv: 6,
      pageViewMode: 'stateChange', // stateChange or vpv
    }),
    amplitudePlugin({ apiKey: '1abb7c0fc7666f8fe8172e330e4d1622' }),
  ],
});

analytics.on('track', ({ payload }) => {
  console.log('Event Dispatch: ', payload);
});

analytics.on('page', ({ payload }) => {
  console.log('Page: ', payload);
});
analytics.on('identify', ({ payload }) => {
  console.log('Identify: ', payload);
});

/* In app, use .on/.once listeners */
analytics.on('windowEnter', () => {
  console.log('WindowEnter');
});

analytics.on('windowLeft', () => {
  console.log('WindowLeft');
});

/* Or fire .on listeners */
analytics.on('tabHidden', () => {
  console.log('tabHidden');
});
analytics.on('tabVisible', () => {
  console.log('tabVisible');
});

window.Analytics = analytics;
/* export the instance for usage in your app */
export default analytics;
