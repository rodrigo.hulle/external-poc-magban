const config = {
  siteId: '',
  debug: false,
  sv: 6,
  pageViewMode: 'stateChange', // stateChange or vpv
};

/* Export the integration */
export default function hotjarPlugin(userConfig) {
  // Allow for userland overides of base methods
  return {
    NAMESPACE: 'hotjar',
    config: {
      ...config,
      ...userConfig,
    },
    initialize: ({ config }) => {
      if (!config.siteId) {
        throw new Error('No site Id supplied for Hotjar');
      }
      /* eslint-disable */
      if (!scriptAlreadyLoaded()) {
        (function (h, o, t, j, a, r) {
          h.hj =
            h.hj ||
            function () {
              (h.hj.q = h.hj.q || []).push(arguments);
            };
          h._hjSettings = {
            hjid: config.siteId,
            hjsv: config.sv,
            hjdebug: config.debug,
          };
          h._scriptPath = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
          if (!document.querySelector('script[src*="' + h._scriptPath + '"]')) {
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = h._scriptPath;
            a.appendChild(r);
          }
        })(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
      }
      /* eslint-enable */
    },
    /*
     * Para o page view funcionar temos que informar manualmente ao hotjar:
     *
     * https://help.hotjar.com/hc/en-us/articles/360034378534#change_3
     * Ou podemos usar um Virtual pageView TODO: ISC
     * https://help.hotjar.com/hc/en-us/articles/115011805448
     */

    page: ({ payload, config }) => {
      window.hj(config.pageViewMode, payload.properties.path);
    },
    /* track event */
    track: ({ payload, config }) => {
      window.hj('trigger', payload.event);
    },
    /* identify user
     * Vamos implantar mas os atributos só servem de filtro no bussiness plane.
     * vamos acessar o userID da getanalytics.io, usaremos o anonimo para tudo, até o momento de identify
     * https://help.hotjar.com/hc/en-us/articles/360038394053
     * hj('identify', userId, {
     *   user_attribute: value
     * });
     */
    identify: ({ payload }) => {
      window.hj('identify', payload.traits.clientMagbanId, {
        name: payload.traits.clientName,
        company: payload.traits.clientCompanyName,
        executive: payload.traits.salesExecutiveName,
      });
    },
    /* Verify script loaded */
    // Retornando hj pra ver se eu posso chamar ele nos eventos depois
    loaded: function () {
      return !!window.hj;
    },
  };
}

// <!-- Hotjar Tracking Code for https://gatsbyks-1472188443.gtsb.io/ -->
// <script>
//     (function(h,o,t,j,a,r){
//         h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
//         h._hjSettings={hjid:1747839,hjsv:6};
//         a=o.getElementsByTagName('head')[0];
//         r=o.createElement('script');r.async=1;
//         r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
//         a.appendChild(r);
//     })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
// </script>

function scriptAlreadyLoaded() {
  const scripts = document.getElementsByTagName('script');
  return !!Object.keys(scripts).filter((key) => {
    const { src } = scripts[key];
    return src.match(/hotjar\.com\/c\/hotjar-/);
  }).length;
}
