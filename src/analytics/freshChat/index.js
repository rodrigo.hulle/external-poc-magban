export default function freshChatPlugin(userConfig) {
  // return object for analytics to use
  return {
    name: 'freshchat-plugin',
    config: {
      whatEver: userConfig.whatEver,
      elseYouNeed: userConfig.elseYouNeed,
    },
    initialize: ({ config }) => {},
    page: ({ payload }) => {},
    track: ({ payload: { event, properties } }) => {
      window.fcWidget.track(event, properties);
    },
    identify: ({ payload }) => {
      window.fcWidget.user.setProperties({
        firstName: payload.traits.clientName,
        email: payload.traits.clientEmail,
        phone: payload.traits.clientPhone,
        magbanId: payload.traits.clientMagbanId,
        company: payload.traits.clientCompanyName,
      });
    },
    loaded: () => {
      // return boolean so analytics knows when it can send data to third party
      return !!window.fcWidget;
    },
  };
}

function scriptAlreadyLoaded() {
  const scripts = document.getElementsByTagName('script');
  return !!Object.keys(scripts).filter((key) => {
    const { src } = scripts[key];
    return src.match('freshchat.com/js/widget');
  }).length;
}
