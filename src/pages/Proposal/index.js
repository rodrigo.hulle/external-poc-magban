import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { Normalize } from '@smooth-ui/core-sc';
import { ThemeProvider } from 'styled-components';
import SimpleReactLightbox from 'simple-react-lightbox';

import { theme, GlobalStyles } from '../../theme/config';

import Card from '../../components/Card/index';
import CardGroup from '../../components/Card/CardGroup';
import WrapperContent from '../../components/Layout/WrapperContent';
import Footer from '../../components/Footer';
import Hero from '../../components/Hero';
import Navbar from '../../components/Navigation/Navbar';
import FreshChat from 'react-freshchat';
import { Beforeunload } from 'react-beforeunload';
import { useAnalytics } from 'use-analytics';

export default function Proposal({
  title,
  clientData,
  description,
  catalogItens,
  ...props
}) {
  const { track, identify } = useAnalytics();
  function handleUnload() {
    track('Catalog Closed');
  }

  useEffect(() => {
    identify(clientData.clientMagbanId, clientData);
    track('Catalog Opened');
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className='App'>
      <Beforeunload onBeforeunload={handleUnload}>
        <Helmet>
          <link
            rel='preload'
            href='https://fonts.googleapis.com/css?family=Roboto&display=swap'
            as='style'
          />
          <link
            rel='stylesheet'
            href='https://fonts.googleapis.com/css?family=Roboto&display=swap'
          />
        </Helmet>
        <Normalize />
        <GlobalStyles />

        <ThemeProvider theme={theme}>
          <SimpleReactLightbox>
            <WrapperContent>
              <Navbar />
              <Hero
                clientData={clientData}
                title={title}
                description={description}
              />
              <CardGroup>
                {catalogItens.map(
                  ({ id, materialInfo, offerData, slabInfo, images }) => {
                    const key = `${id}`;
                    return (
                      <Card
                        key={key}
                        ownQuarry={materialInfo.isOwnQuarry}
                        category={materialInfo.materialCategory}
                        type={materialInfo.materialType}
                        name={materialInfo.materialName}
                        value={offerData.unitaryValue}
                        images={images}
                        slabInfo={slabInfo}
                      />
                    );
                  }
                )}
              </CardGroup>
              <Footer />
              <FreshChat
                token={'d60e9117-ccca-4a74-bd83-22db84d1d4fc'}
                onInit={(widget) => {}}
              />
            </WrapperContent>
          </SimpleReactLightbox>
        </ThemeProvider>
      </Beforeunload>
    </div>
  );
}
