import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Proposal from './pages/Proposal';
import Data from './data/data';
import { Beforeunload } from 'react-beforeunload';
import { useAnalytics } from 'use-analytics';

function App() {
  const { track } = useAnalytics();

  function handleUnload() {
    track('Unload');
  }

  const data = Data();

  const pages = data.map((page) => (
    <Route path={`/${page.url}`} key={page.id}>
      <Proposal {...page} />
    </Route>
  ));

  return (
    <Beforeunload onBeforeunload={handleUnload}>
      <Router>
        <Switch>{pages}</Switch>
      </Router>
    </Beforeunload>
  );
}

export default App;
