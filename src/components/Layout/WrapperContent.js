import React from 'react'
import styled, { Box } from '@xstyled/styled-components';

const StyledBox = styled(Box)`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`

const WrapperContent = ({children, ...props}) => {
  return (
    <StyledBox {...props}>
      {children}
    </StyledBox>
  )
}

export default WrapperContent
