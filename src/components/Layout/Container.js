import React from 'react'
import { breakpoints } from '@xstyled/system'
import styled, { Box, css } from '@xstyled/styled-components'

import t from 'prop-types'

const ContainerBox = styled(Box)(
  breakpoints({
    xs: css`
      margin: 0 auto;
      padding-right: 5;
      padding-left: 5;
    `,
    sm: css`
      width: 540px;
      margin: 0 auto;
    `,
    md: css`
      width: 720px;
      margin: 0 auto;
    `,
    lg: css`
      width: 960px;
      margin: 0 auto;
    `,
    xl: css`
      width: 1140px;
      margin: 0 auto;
    `,
  }),
)

const ContainerFluid = styled(Box)`
  width: 100%;
  padding-right: 5;
  padding-left: 5;
`

export const Container = ({children, isFluid, ...rest}) => {
  const content = [];

  if (isFluid === true) {
    content.push(
      <ContainerFluid key='container-fluid' {...rest}>
        {children}
      </ContainerFluid>
    )
  } else {
    content.push(
      <ContainerBox key='container-box' {...rest}>
        {children}
      </ContainerBox>
    )
  }
  return content
}


Container.propTypes = {
  isFluid: t.bool
}

Container.defaultProps = {
  isFluid: false
}
