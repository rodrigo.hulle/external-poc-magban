import React from 'react';
import t from 'prop-types';
import { breakpoints } from '@xstyled/system';
import styled, { Box, css } from '@xstyled/styled-components';

import { TextBase } from '../DataDisplay/TextBase';
import { Container } from '../Layout/Container';

const WrapperContent = styled(Box)`
  margin-top: 12px;
  max-width: 900px;

  h1,
  p {
    line-height: 1.25;
  }

  h1 {
    position: relative;
    padding-bottom: 36px;

    &:after {
      position: absolute;
      display: block;
      content: '';
      width: 45px;
      height: 6px;
      margin-top: 20px;
      border-radius: 5px;
      background-color: primary;
    }
  }

  p {
    color: rgb(112, 112, 112);
    font-size: 18px;
  }
`;

const StyledBox = styled(Box)`
  background-color: lightGray;
  margin-bottom: 42px;
  ${breakpoints({
    xs: css`
      padding: 6rem 0 2rem;

      & > div {
        padding: 0 2rem;
      }
    `,
    sm: css`
      padding: 120px 0 60px; /* large padding top grants correct positioning according to navbar */
    `,
  })}
`;

const Hero = ({ title, clientData, description, ...props }) => {
  return (
    <StyledBox {...props}>
      <Container>
        <WrapperContent>
          <TextBase level='h1'>{title}</TextBase>
          <TextBase type='lead'>
            {clientData.clientName}, você recebeu uma nova oferta de{' '}
            {clientData.salesExecutiveName}. <br />
            {description}
          </TextBase>
          <TextBase type='lead'>
            In Order to improve the next offers, check your level of interest in
            the items, using our rating system.
          </TextBase>
        </WrapperContent>
      </Container>
    </StyledBox>
  );
};

Hero.propTypes = {
  clientData: t.object,
  title: t.string,
};

export default Hero;
