import React from 'react'
import t from 'prop-types'

import styled, { css } from '@xstyled/styled-components'
import { variant } from '@xstyled/system'


const colorVariant = variant({
  prop: 'color',
  default: 'inherit',
  variants: {
    inherit:   css ` color: inherit; `,
    dark:      css ` color: dark; `,
    light:     css ` color: light; `,
    white:     css ` color: white; `,

    primary:   css ` color: primary; `,
    secondary: css ` color: secondary; `,

    warning:   css ` color: warning; `,
    success:   css ` color: success; `,
    danger:    css ` color: danger; `,
    info:      css ` color: info; `,
  }
})

const typeVariant = variant({
  prop: 'type',
  default: 'description',
  variants: {
    heading: css`
      font-family: title;
    `,
    description: css`
      font-family: base;
    `,
    lead: css`
      font-family: base;
      font-size: 3;
    `
  }
})

const transformVariant = variant({
  prop: 'transform',
  default: 'none',
  variants: {
    none: css`
      text-transform: none;
    `,
    uppercase: css`
      text-transform: uppercase;
    `
  }
})


const StyledText = styled.p`
  color: bodyColor;

  ${colorVariant}
  ${typeVariant}
  ${transformVariant}
`

export const TextBase = ({ children, level, ...props }) => {
  let type = ''

  if(level === 'lead') {
    type = level
    level = 'p'
  } else if (['h1', 'h2', 'h3', 'h4', 'h5', 'h6' ].includes(level)) {
    type = 'heading'
  }
  else {
    type = 'description'
  }
  return (
    <StyledText type={type} as={level} {...props}>
      {children}
    </StyledText>
  )
};

TextBase.propTypes = {
  children: t.any,
  color: t.oneOfType([t.string, t.oneOf([
    'inherit',
    'dark',
    'light',
    'primary',
    'secondary',
    'warning',
    'success',
    'danger',
    'info'
  ])]),
  level: t.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p', 'span', 'label']),
  transform: t.oneOf(['none', 'uppercase']),
}

TextBase.defaultProps = {
  children: '',
  level: 'p',
}
