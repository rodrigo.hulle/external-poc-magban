import React, { useState, useRef, useEffect } from 'react';
import Card from '../../components/Card';

// doing mother fucker visibility tracking

const ProposalCards = ({ catalogItens }) => {
  const printCard = catalogItens.map((catalogItem) => (
    <Card
      key={catalogItem.id}
      ownQuarry={catalogItem.materialInfo.isOwnQuarry}
      category={catalogItem.materialInfo.materialCategory}
      type={catalogItem.materialInfo.materialType}
      name={catalogItem.materialInfo.materialName}
      value={catalogItem.offerData.unitaryValue}
      images={catalogItem.images}
      slabInfo={catalogItem.slabInfo}
    />
  ));

  return <>{printCard}</>;
};

export default ProposalCards;
