import React, { useState } from 'react'
import t from 'prop-types'
import { variant } from '@xstyled/system'
import styled, { Box, css } from '@xstyled/styled-components'
import { ThumbUp, ThumbDown } from '@styled-icons/material'
import { VisuallyHidden } from "reakit/VisuallyHidden";

import Button from '../Button'


const StyledButton = styled(Button)`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  padding: 6px;
  height: 30px;
  width: 30px;
  border-radius: 50%;

  ${variant({
    key: 'color',
    default: 'dark',
    variants: {
      dark: css`
        color: black;
        border-color: black;
      `,
      success: css`
        color: success;
        border-color: success !important;
      `,
      danger: css`
        color: danger;
        border-color: danger !important;
      `
    }
  })}
`

const ButtonIcon = ({icon, ...props}) => {
  const [clicked, setClicked] = useState(false);

  if (icon === 'thumbsDown') {
    return (
      <StyledButton outline onClick={() => setClicked(!clicked)} color={clicked ? 'danger' : 'dark'} {...props}>
        <VisuallyHidden>Não</VisuallyHidden>
        <ThumbDown size="18" />
      </StyledButton>
    )
  } else {
    return(
      <StyledButton outline onClick={() => setClicked(!clicked)} color={clicked ? 'success' : 'dark'} {...props}>
        <VisuallyHidden>Sim</VisuallyHidden>
        <ThumbUp size="18" />
      </StyledButton>
    )
  }
}

ButtonIcon.propTypes={

}


export default ButtonIcon
