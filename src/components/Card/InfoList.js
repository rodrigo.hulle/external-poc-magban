import React from 'react'
import t from 'prop-types'
import styled from '@xstyled/styled-components'


const Info = styled.ul`
  margin-top: 0;
  padding: 0;
  list-style: none;

  li {
    font-size: 14px;
    padding: 6px 0;
  }
`

const InfoList = ({ items, ...props }) => {
  return (
    <Info {...props}>
      {items.map((item, index)=>(
        <li key={`infoList-${index}`}>
          <strong>{item.title}:</strong> {item.info}
        </li>
      ))}
    </Info>
  )
}

InfoList.propTypes = {
  items: t.arrayOf(t.shape({
    title: t.string,
    info: t.string
  }))
}

export default InfoList
