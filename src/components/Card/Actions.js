import React, { useState, useEffect } from 'react';
import { breakpoints } from '@xstyled/system';
import styled, { Box, css } from '@xstyled/styled-components';

import Button from '../Button';

const ButtonGroup = styled(Box)`
  display: flex;
  justify-content: flex-end;
  max-height: 36px;

  ${breakpoints({
    xs: css`
      width: 100%;
      text-align: center;
      margin-top: 30px;

      & > * {
        width: 50%;
      }
    `,
  })}
`;

const Actions = ({ onChange, ...props }) => {
  const [approved, setApproved] = useState(null);

  useEffect(() => {
    if (approved !== null) {
      onChange(approved);
    }
  }, [approved]);

  return (
    <ButtonGroup {...props}>
      <Button
        outline
        marginRight={4}
        onClick={() => setApproved(false)}
        variant={approved === false ? 'danger' : 'dark'}
        {...props}>
        Recusar
      </Button>
      <Button
        onClick={() => setApproved(true)}
        variant={approved === true ? 'success' : 'dark'}>
        Aceitar
      </Button>
    </ButtonGroup>
  );
};

export default Actions;
