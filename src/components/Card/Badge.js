import React from 'react'
import styled, { Box, css } from '@xstyled/styled-components'
import { breakpoints } from '@xstyled/system';
import { TextBase } from '../DataDisplay/TextBase'

const StyledBox = styled(Box)`
  position: absolute;
  font-weight: 700;
  background-color: primary;

  p {
    margin: 0;
    color: white;
  }

  &:after {
    z-index: 0;
    content: '';
    display: block;
    position: absolute;
    background: transparent;
    border-style: solid;
    border-top-color: #0f0701;
    border-color: transparent;
    transform: rotate(45deg);
  }

  ${breakpoints({
    xs: css`
      left: -9px;
      top: -54px;
      padding: 0.5rem 1.15rem;

      &:after {
        left: 3px;
        top: 21px;
        border-width: 6px;
        border-bottom: 6px solid #791116;
      }

      p {
        font-size: 10px;
        letter-spacing: 2px;
      }
    `,
    sm: css`
      left: -21px;
      top: -72px;
      padding: 0.7rem 1.75rem;

      &:after {
        left: 6px;
        top: 21px;
        border-width: 15px;
        border-bottom: 15px solid #791116;
      }

      p {
        font-size: 12px;
        letter-spacing: 1px;
      }
    `
  })}
`

const Badge = ({label, ...props}) => {
  return (
    <StyledBox {...props}>
      <TextBase transform='uppercase' color='primary' variant='h4'>{label}</TextBase>
    </StyledBox>
  )
}

export default Badge
