import React from 'react'
import { SRLWrapper } from 'simple-react-lightbox'

const options = {
  buttons: {
    iconPadding: '7px',
    iconColor: 'rgba(179, 175, 143, 0.8)'
  },
  caption: {
    captionFontFamily: 'Montserrat, sans-serif',
    captionFontSize: '22px',
    captionColor: '#8D99AE',
    captionFontWeight: 300,
    showCaption: true
  },
  settings: {
    overlayColor: 'rgba(43, 45, 66, 0.95)',
    transitionTimingFunction: 'ease-in-out',
    slideTransitionSpeed: 0.6,
    slideTransitionTimingFunction: [0.25, 0.75, 0.5, 1],
    slideAnimationType: 'slide',
    enablePanzoom: false,
    autoplaySpeed: 5000,
    hideControlsAfter: false
  },
  progressBar: {
    height: '3px',
    fillColor: '#8D99AE',
    backgroundColor: 'rgba(43, 45, 66, 0.95)'
  },
  thumbnails: {
    thumbnailsSize: ['150px', '100px'],
    thumbnailsGap: '0 5px'
  }
}


const Lightbox = ({children, images, ...props}) => {
  return (
    <SRLWrapper images={images} options={options} {...props}>
      {children}
    </SRLWrapper>
  )
}

export default Lightbox
