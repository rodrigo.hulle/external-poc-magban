import React from 'react'
import { breakpoints } from '@xstyled/system'
import styled, { Box, css } from '@xstyled/styled-components'


const StyledBox = styled(Box)`
  display: flex;
  flex-wrap: wrap;

  & > div {
    margin-bottom: 5;
    margin-left: auto;
    margin-right: auto;
  }

  ${breakpoints({
    xs: css`
      margin: 0 auto;
      padding-right: 2;
      padding-left: 2;
    `,
    sm: css`
      width: 540px;
      margin: 0 auto;
    `,
    md: css`
      width: 720px;
      margin: 0 auto;
    `,
    lg: css`
      width: 100%;
      margin: 0 auto;
      justify-content: space-evenly;
    `,
  })}
`

const CardGroup = ({children}) => {
  return (
    <StyledBox>
      {children}
    </StyledBox>
  )
}

export default CardGroup
