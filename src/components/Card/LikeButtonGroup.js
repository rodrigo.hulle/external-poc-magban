import React, { useState, useEffect } from 'react';
import styled from '@xstyled/styled-components';
import { ThumbUp, ThumbDown } from '@styled-icons/material';
import { VisuallyHidden } from 'reakit/VisuallyHidden';

import Button from '../Button';

const StyledButton = styled(Button)`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  padding: 6px;
  height: 30px;
  width: 30px;
  border-radius: 50%;
`;

const LikeButtonGroup = ({ icon, onChange, ...props }) => {
  const [liked, setLiked] = useState(null);

  useEffect(() => {
    if (liked !== null) {
      onChange(liked);
    }
  }, [liked]);

  return (
    <>
      <StyledButton
        marginRight={4}
        outline
        onClick={() => setLiked(true)}
        variant={liked === true ? 'success' : 'dark'}
        {...props}>
        <VisuallyHidden>Sim</VisuallyHidden>
        <ThumbUp size='18' />
      </StyledButton>
      <StyledButton
        outline
        onClick={() => setLiked(false)}
        variant={liked === false ? 'danger' : 'dark'}
        {...props}>
        <VisuallyHidden>Não</VisuallyHidden>
        <ThumbDown size='18' />
      </StyledButton>
    </>
  );
};

LikeButtonGroup.propTypes = {};

export default LikeButtonGroup;
