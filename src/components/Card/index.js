import React, { useEffect } from 'react';
import t from 'prop-types';
import { th, breakpoints } from '@xstyled/system';
import styled, { Box, css } from '@xstyled/styled-components';
import { useAnalytics } from 'use-analytics';

import {
  Card as CardBase,
  CardBody as CardBodyBase,
  CardTitle,
  CardImg,
} from '@smooth-ui/core-sc';

import Actions from './Actions';
import Badge from './Badge';
import Carousel from './Carousel';
import InfoList from './InfoList';
import Lightbox from './Lightbox';
import LikeButtonGroup from './LikeButtonGroup';
import { TextBase } from '../DataDisplay/TextBase';

const CardStyled = styled(CardBase)`
  box-shadow: 0 0 6px 0 rgba(0, 0, 0, 0.2);

  ${breakpoints({
    sm: css`
      max-width: 420px;
    `,
    xs: css`
      max-width: 270px;
    `,
  })}
`;

const Type = styled(TextBase)`
  margin: 0;
  letter-spacing: 1px;
  font-weight: 700;
  font-size: 12px;
`;

const Title = styled(TextBase)`
  font-family: ${`'` + th('fonts.title') + `'`}, ${`'` + th('fonts.base') + `'`},
    ${th('fonts.fallback')};
  font-weight: 800;
  margin: 0;

  ${breakpoints({
    xs: css`
      font-size: 20px;
    `,
    md: css`
      font-size: 28px;
    `,
  })}
`;

const CardBody = styled(CardBodyBase)`
  padding: 0;

  & > div:nth-of-type(1) {
    padding-bottom: 0;
    border-bottom: 1px solid #e0e0e0;
  }

  & > div:nth-of-type(2) {
    padding-top: 0;
  }

  ${breakpoints({
    xs: css`
      & > div {
        padding: 1.25rem 1rem;
      }
    `,
    md: css`
      & > div {
        padding: 1.5rem;
        padding-bottom: calc(1.5rem + 90px);
        display: block;
      }
    `,
  })}
`;

const StyledBox = styled(Box)`
  ${breakpoints({
    xs: css``,
    md: css`
      display: flex;
      align-items: flex-end;
      position: absolute;
      bottom: 1.5rem;
      right: 0;
      left: 0;
      padding: 0 1.5rem;
    `,
  })}
`;

const Small = styled(TextBase)`
  font-size: 12px;
`;

const Card = ({
  id,
  ownQuarry,
  images,
  type,
  category,
  name,
  value,
  slabInfo,
}) => {
  const { track } = useAnalytics();

  function getItem() {
    var item = {
      materialName: name,
      materialCategory: category,
      materialType: type,
      isOwnQuarry: ownQuarry,
      unitaryValue: value,
      slabsInfo: slabInfo,
    };
    return item;
  }

  useEffect(() => {
    track('Item Printed', getItem());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function handlePreference(status) {
    var item = {
      ...getItem(),
      preferenceType: status ? 'More' : 'Less',
    };
    track('Item Preference Recorded', item);
  }

  function handleOfferAcceptance(status) {
    var item = {
      ...getItem(),
      preferenceType: status ? 'Accepted' : 'Declined',
    };
    status ? track('Item Accepted', item) : track('Item Declined', item);
  }
  function handleCarousel() {
    track('Carousel');
  }

  function handleLightBox() {
    track('Item Gallery Viewed', getItem());
  }
  return (
    <CardStyled>
      <Lightbox images={images}>
        <Carousel>
          {images.map((img, index) => (
            <CardImg
              key={`cardImg-${index}`}
              borderRadius={0}
              src={img.src}
              alt={img.caption}
              onClick={handleLightBox}
            />
          ))}
        </Carousel>
      </Lightbox>
      <CardBody>
        <CardTitle position='relative'>
          {ownQuarry && <Badge label='Own Quarry' />}
          <Type transform='uppercase' color='primary' variant='h4'>
            {category}
          </Type>
          <Box display='flex' justifyContent='space-between'>
            <Title variant='h3'>{name}</Title>
            <Title variant='h3'>{value}$/m²</Title>
          </Box>
          <TextBase transform='uppercase' forwardedAs='p'>
            REF: {slabInfo.ref} | {slabInfo.thickness} - {slabInfo.finishing}
          </TextBase>
        </CardTitle>
        <Box>
          <InfoList
            items={[
              {
                title: 'O cavalete contém',
                info:
                  slabInfo.qty +
                  ' chapas de ' +
                  slabInfo.height +
                  'x' +
                  slabInfo.width +
                  'm',
              },
              {
                title: 'Área total',
                info: slabInfo.area + 'm²',
              },
              {
                title: 'Peso',
                info: slabInfo.weight + 'kg',
              },
            ]}
          />
          <StyledBox>
            <Box>
              <Small forwardedAs='p'>
                Gostaria de receber mais ofertas deste material?
              </Small>
              <Box display='inline-block'>
                <LikeButtonGroup onChange={handlePreference} />
              </Box>
            </Box>
            <Actions onChange={handleOfferAcceptance} />
          </StyledBox>
        </Box>
      </CardBody>
    </CardStyled>
  );
};

Card.propTypes = {
  type: t.string,
  name: t.string,
  value: t.string,
  ownQuarry: t.bool,
  slabInfo: t.shape({
    qty: t.string,
    height: t.string,
    width: t.string,
    area: t.string,
    weight: t.string,
  }),
};

export default Card;
