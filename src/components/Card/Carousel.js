import React from 'react';
import styled from '@xstyled/styled-components';

import BaseCarousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import analytics from '../../analytics/analytics';

const CustomCarousel = styled(BaseCarousel)`
  & > button {
    position: absolute;
    background-color: transparent;
    z-index: 1;

    &:first-of-type {
      left: 0;
    }

    &:last-of-type {
      right: 0;
    }

    &:hover:enabled {
      background-color: rgba(255, 255, 255, 0.5);
    }
  }

  & + ul {
    position: relative;
    z-index: 1;
    margin-top: -25px;

    li > button[class*='selected'],
    li > button {
      background-color: transparent;

      &:before {
        background: white !important;
      }
    }
  }
`;

const Carousel = ({ children, ...props }) => {
  return (
    <CustomCarousel
      dots
      infinite
      arrows
      slidesPerPage={1}
      itemWidth={420}
      breakpoints={{
        320: {
          itemWidth: 270
        }
      }}
      {...props}>
      {children}
    </CustomCarousel>
  );
};

export default Carousel;
