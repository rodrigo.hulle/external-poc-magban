import React from 'react'
import styled, { Box } from '@xstyled/styled-components'

import Link from '../Navigation/Link'
import { TextBase } from '../DataDisplay/TextBase'


const StyledBox = styled(Box)`
  padding: 5 0;
  margin-top: auto;
  background-color: secondary;
  text-align: center;

  & > p {
    font-size: 14px;
    color: #fafafa;
  }
`

const WrapperBrand = styled(Link)`
  opacity: 1;
`

const Footer = () => {
  return (
    <StyledBox>
      <WrapperBrand href='/'>
        <img src='https://magban.com/static/7a14a9bbc06d96a2b3dff14dead558e2/34da4/magban-logo-white.png' alt='Magban | Logo Negative | Natural Inspiration' />
      </WrapperBrand>
      <TextBase type='lead'>Magban 2020. Todos os direitos reservados.</TextBase>
    </StyledBox>
  )
}

export default Footer
