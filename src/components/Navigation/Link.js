import React from 'react'
import t from 'prop-types'
import styled, { css } from '@xstyled/styled-components'
import { variant } from '@xstyled/system'

import Button from '../Button'

const color = variant({
  default: 'info',
  prop: 'color',
  variants: {
    primary: css`
      color: primary;
    `,
    secondary: css`
      color: secondary;
    `,
    info: css`
      color: info;
    `,
    success: css`
      color: success;
    `,
    light: css`
      color: light;
    `,
    white: css`
      color: white;
    `,
    dark: css`
      color: dark;
    `,
  }
})

const CommonStyles = css`
  ${color}
  opacity: .75;
  text-decoration: none;

  &:hover {
    cursor: pointer;
    opacity: 1;
    outline: 0;
  }

  &,
  &:hover,
  &:focus {
    transition: opacity ease-in-out .2s;
  }
`

const StyledLink = styled.a`
  ${CommonStyles}
`

const Link = ({children, variant, href, target, color, ...props}) => {
  if(variant === 'button') {
    return (
      <Button target={target} forwardedAs='a' href={href} variant={color} outline={props.outline} {...props}>
        {children}
      </Button>
    )
  } else {
    return (
      <StyledLink target={target} href={href} color={color} {...props}>
        {children}
      </StyledLink>
    )
  }
}

Link.propTypes = {
  children: t.any,
  href: t.string,
  target: t.oneOf(['_self', '_blank']),
  variant: t.oneOf(['default', 'button']),
  color: t.oneOf(['primary', 'secondary', 'success', 'info', 'white', 'light', 'dark'])
}

Link.defaultProp = {
  target: '_self',
  variant: 'default'
}

export default Link