import React from 'react'
import styled, { Box, css } from '@xstyled/styled-components'
import { variant } from '@xstyled/system'
import t from 'prop-types'

import Link from './Link'


const colorVariants = variant({
  default: 'dark',
  prop: 'color',
  variants: {
    primary: css`
      a {
        opacity: .75;
        color: primary;
      }
      a:hover {
        opacity: 1;
        color: primary;
      }
    `,
    light: css`
      a {
        opacity: .75;
        color: light;
      }
      a:hover {
        opacity: 1;
        color: light;
      }
    `,
    white: css`
      a {
        opacity: .75;
        color: white;
      }
      a:hover {
        opacity: 1;
        color: white;
      }
    `,
    dark: css`
      a {
        opacity: .75;
        color: bodyColor;
      }
      a:hover {
        opacity: 1;
        color: dark;
      }
    `,
  }
})

const displayVariant = variant({
  default: 'block',
  prop: 'display',
  variants: {
    flex: css`
      ul {
        display: flex;
      }
    `,
    block: css`
      ul {
        display: block;
      }
    `,
  }
})

const MenuWrapper = styled(Box)`
  padding: 1;
  display: block;

  ${colorVariants}
  ${displayVariant}

  h6 {
    text-transform: uppercase;
    font-size: 12px;
    letter-spacing: 0.75px;
    margin-bottom: 3;
  }
`

const MenuList = styled(Box)`
  list-style: none;
  padding-left: 0;
`

const MenuItem = styled.li`
  a {
    display: inline-block;
    padding: 2 0;
    margin-bottom: 1;
    opacity: .75;
    text-decoration: none;
  }

  a:hover {
    opacity: 1;
    text-decoration: none;
  }
`

export const Menu = ({items, color, title, linkProps, ...rest}) => {
  return (
    <MenuWrapper color={color} {...rest}>
      {title && ( <h6>{title}</h6> )}
      <MenuList forwardedAs='ul'>
        {items.map((item, index) => (
          <MenuItem key={`menuItem-${index}`}>
            <Link href={item.href} {...item}>
              {item.label}
            </Link>
          </MenuItem>
        ))}
      </MenuList>
    </MenuWrapper>
  )
}

Menu.propTypes = {
  items: t.arrayOf(t.shape({
    url: t.string,
    label: t.string
  })),
  title: t.oneOfType([t.bool, t.string]),
  color: t.string
}

Menu.defaultProp = {
  title: false,
}
