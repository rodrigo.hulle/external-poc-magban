import React from 'react'

// import  from '@xstyled/system'
import styled, { Box, up, down, css } from '@xstyled/styled-components'
import { Container } from '../Layout/Container'

import { Menu } from './Menu'
import Link from './Link'

const StyledNavbar = styled(Box)`
  width: 100%;
  position: fixed;
  z-index: 1080;
  top: 0;
  background-color: white;
  box-shadow: 0 0 16px 0 rgba(0,0,0,0.15);
  visibility: 'visible';

  & > div {
    display: flex;
    align-items: center;
    height: 100%;
  }

  ${up(
    'lg',
    css`
      height: 72px;
    `
  )}

  ${down(
    'lg',
    css`
      height: 60px;
    `
  )}
`


const StyledMenu = styled(Menu)`
  align-items: center;

  ul {
    li {
      padding: 1 4;

      a {
        margin-bottom: 0;
      }
    }
  }


  ${up(
    'lg',
    css`
      display: flex !important;

      ul {
        display: flex !important;
        margin: auto 0;
      }
    `
  )}
`

const Nav = styled(Box)`
  display: flex;
  justify-content: flex-end;

  & > :last-child {
    margin-left: 4;
  }
`

const WrapperBrand = styled(Link)`
  opacity: 1;
  ${down(
    'lg',
    css`
      display: block;
      margin: 0 auto;
    `
  )}
`

const LinkButton = styled(Link)`
  &,
  & > * {
    line-height: 4; /* this value === 2, according to theme definitions */
  }
`

const Navbar = ({brandProps, navItems, ctaProps, ...props}) => {
  return (
    <StyledNavbar {...props}>
      <Container py='0'>
        <WrapperBrand href='/'>
          <img src='https://magban.com/static/be2b89a8492be0f2a154326a2f12b446/ca512/magban-logo-navbar.png' alt='Magban | Natural Inspiration' />
        </WrapperBrand>
        {navItems && (
          <Nav marginLeft='auto'>
            <StyledMenu color='light' items={navItems} />
            {ctaProps && <LinkButton outline variant='button' href={ctaProps.href} {...ctaProps}>{ctaProps.label}</LinkButton>}
          </Nav>
        )}
      </Container>
    </StyledNavbar>
  )
}

export default Navbar
