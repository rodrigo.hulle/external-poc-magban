import React from 'react'
import styled from '@xstyled/styled-components'
import { th } from '@xstyled/system'

import { Button as BaseButton } from '@smooth-ui/core-sc'

const StyledButton = styled(BaseButton)`
  font-family:  ${`'`+th('fonts.title')+`'`}, ${`'`+th('fonts.base')+`'`}, ${th('fonts.fallback')};
  font-size: 14px;
  text-transform: uppercase;
  letter-spacing: 1.5px;
  font-weight: 700;
`

const Button = ({children, ...props}) => {
  return (
    <StyledButton {...props}>
      {children}
    </StyledButton>
  )
}

Button.propTypes = {

}

export default Button
