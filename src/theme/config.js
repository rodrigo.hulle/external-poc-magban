import { createGlobalStyle } from 'styled-components'

const theme = {
  colors: {
    primary: '#CB2229',
    secondary: '#272525',
    lightGray: '#F5F5F5',

    dark: '#212529',
    bodyColor: '#707070',
    green: '#66BB6A'
  },

  fonts: {
    fallback: `'Open Sans','Helvetica neue', Arial, sans-serif`,
    base: 'Roboto',
    title: 'Poppins',
  },
}

const GlobalStyles = createGlobalStyle` /* Global Styles goes here! */
  html,
  body {
    font-family: ${`'`+theme.fonts.base+`'`}, ${theme.fonts.fallback};
    background-color: #FFFFFF;
  }

  p {
    font-family: ${`'`+theme.fonts.base+`'`}, ${theme.fonts.fallback};
  }

  div#SRLLightbox {
    z-index: 1081; /* corrects layer according to navbar lvl */
  }

  h1, h2, h3 {
    font-family: ${`'`+theme.fonts.title+`'`}, ${`'`+theme.fonts.base+`'`}, ${theme.fonts.fallback};
  }
`


export { theme, GlobalStyles }