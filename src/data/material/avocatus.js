const Avocatus = () => {
  return {
    id: 2,
    materialInfo: {
      materialType: 'Bundle',
      materialName: 'Avocatus',
      materialCategory: 'Prime',
      isOwnQuarry: true,
    },
    offerData: {
      unitaryValue: '108 ',
    },
    slabInfo: {
      ref: '00017540 - 102698',
      qty: '6',
      height: '1.86',
      width: '3.19',
      weight: '5500',
      area: '111.58',
      thickness: '3.0 CM',
      finishing: 'POLISHED FINISH',
    },
    images: [
      {
        id: 21,
        src: './TinyPNG-1920x1080/Avocatus/avocatus-02-lg.png',
        thumbnail: './TinyPNG-720x480/Avocatus/avocatus-02-sm.png',
        caption: 'Slab Close Up Picture',
      },
      {
        id: 22,
        src: './TinyPNG-1920x1080/Avocatus/avocatus-01-lg.png',
        thumbnail: './TinyPNG-720x480/Avocatus/avocatus-01-sm.png',
        caption: 'Marvelous mix between wood and granite',
      },
      {
        id: 23,
        src: './TinyPNG-1920x1080/Avocatus/avocatus-03-lg.png',
        thumbnail: './TinyPNG-720x480/Avocatus/avocatus-03-sm.png',
        caption: 'Tiny Picture',
      },
      {
        id: 24,
        src: './TinyPNG-1920x1080/Avocatus/avocatus-04-lg.png',
        thumbnail: './TinyPNG-720x480/Avocatus/avocatus-04-sm.png',
        caption: 'Great for use in mutliples surfaces',
      },
      {
        id: 25,
        src: './TinyPNG-1920x1080/Avocatus/avocatus-05-lg.png',
        thumbnail: './TinyPNG-720x480/Avocatus/avocatus-05-sm.png',
        caption: '',
      },
      {
        id: 26,
        src: './TinyPNG-1920x1080/Avocatus/avocatus-06-lg.png',
        thumbnail: './TinyPNG-720x480/Avocatus/avocatus-06-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
      {
        id: 27,
        src: './TinyPNG-1920x1080/Avocatus/avocatus-07-lg.png',
        thumbnail: './TinyPNG-720x480/Avocatus/avocatus-07-sm.png',
        caption: '',
      },
    ],
  };
};

export default Avocatus;
