const Kozmuz = () => {
  return {
    id: 5,
    materialInfo: {
      materialType: 'Bundle',
      materialName: 'Kozmuz',
      materialCategory: 'Prime',
      isOwnQuarry: true,
    },
    offerData: {
      unitaryValue: '96 ',
    },
    slabInfo: {
      ref: '00017540 - 102698',
      qty: '6',
      height: '1.86',
      width: '3.19',
      weight: '3500',
      area: '55.58',
      thickness: '3.0 CM',
      finishing: 'POLISHED FINISH',
    },
    images: [
      {
        id: 51,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-02-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-02-sm.png',
        caption: 'Slab Close Up Picture',
      },
      {
        id: 52,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-01-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-01-sm.png',
        caption: 'Marvelous mix between wood and granite',
      },
      {
        id: 53,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-03-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-03-sm.png',
        caption: 'Tiny Picture',
      },
      {
        id: 54,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-04-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-04-sm.png',
        caption: 'Great for use in mutliples surfaces',
      },
      {
        id: 5,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-05-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-05-sm.png',
        caption: '',
      },
      {
        id: 56,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-06-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-06-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
      {
        id: 57,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-07-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/Kozmuz-07-sm.png',
        caption: '',
      },
      {
        id: 58,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-08-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-08-sm.png',
        caption: '',
      },
      {
        id: 59,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-09-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/Kozmuz-09-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
      {
        id: 510,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-10-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-10-sm.png',
        caption: '',
      },
      {
        id: 511,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-11-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-11-sm.png',
        caption: '',
      },
      {
        id: 512,
        src: './TinyPNG-1920x1080/Kozmuz/kozmuz-12-lg.png',
        thumbnail: './TinyPNG-720x480/Kozmuz/kozmuz-12-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
    ],
  };
};

export default Kozmuz;
