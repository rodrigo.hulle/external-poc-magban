const Michelangelo = () => {
  return {
    id: 7,
    materialInfo: {
      materialType: 'Bundle',
      materialName: 'Michelangelo',
      materialCategory: 'Prime',
      isOwnQuarry: false,
    },
    offerData: {
      unitaryValue: '168 ',
    },
    slabInfo: {
      ref: '00017540 - 102698',
      qty: '6',
      height: '1.86',
      width: '3.19',
      weight: '4500',
      area: '127.58',
      thickness: '3.0 CM',
      finishing: 'POLISHED FINISH',
    },
    images: [
      {
        id: 1,
        src: './TinyPNG-1920x1080/Michelangelo/michelangelo-02-lg.png',
        thumbnail: './TinyPNG-720x480/Michelangelo/michelangelo-02-sm.png',
        caption: 'Slab Close Up Picture',
      },
      {
        id: 2,
        src: './TinyPNG-1920x1080/Michelangelo/michelangelo-01-lg.png',
        thumbnail: './TinyPNG-720x480/Michelangelo/michelangelo-01-sm.png',
        caption: 'Marvelous mix between wood and granite',
      },
      {
        id: 3,
        src: './TinyPNG-1920x1080/Michelangelo/michelangelo-03-lg.png',
        thumbnail: './TinyPNG-720x480/Michelangelo/michelangelo-03-sm.png',
        caption: 'Tiny Picture',
      },
      {
        id: 4,
        src: './TinyPNG-1920x1080/Michelangelo/michelangelo-04-lg.png',
        thumbnail: './TinyPNG-720x480/Michelangelo/michelangelo-04-sm.png',
        caption: 'Great for use in mutliples surfaces',
      },
      {
        id: 5,
        src: './TinyPNG-1920x1080/Michelangelo/michelangelo-05-lg.png',
        thumbnail: './TinyPNG-720x480/Michelangelo/michelangelo-05-sm.png',
        caption: '',
      },
      {
        id: 6,
        src: './TinyPNG-1920x1080/Michelangelo/michelangelo-06-lg.png',
        thumbnail: './TinyPNG-720x480/Michelangelo/michelangelo-06-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
    ],
  };
};

export default Michelangelo;
