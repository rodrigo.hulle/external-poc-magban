const Madeirus = () => {
  return {
    id: 6,
    materialInfo: {
      materialType: 'Bundle',
      materialName: 'Madeirus',
      materialCategory: 'Prime',
      isOwnQuarry: false,
    },
    offerData: {
      unitaryValue: '137.5 ',
    },
    slabInfo: {
      ref: '00017540 - 102698',
      qty: '6',
      height: '1.86',
      width: '3.19',
      weight: '3500',
      area: '67.58',
      thickness: '3.0 CM',
      finishing: 'POLISHED FINISH',
    },
    images: [
      {
        id: 1,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-06-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-06-sm.png',
        caption: 'Slab Close Up Picture',
      },
      {
        id: 2,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-01-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-01-sm.png',
        caption: 'Marvelous mix between wood and granite',
      },
      {
        id: 3,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-03-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-03-sm.png',
        caption: 'Tiny Picture',
      },
      {
        id: 4,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-04-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-04-sm.png',
        caption: 'Great for use in mutliples surfaces',
      },
      {
        id: 5,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-05-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-05-sm.png',
        caption: '',
      },
      {
        id: 6,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-02-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-02-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
      {
        id: 7,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-07-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-07-sm.png',
        caption: '',
      },
      {
        id: 8,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-08-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-08-sm.png',
        caption: '',
      },
      {
        id: 9,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-09-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-09-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
      {
        id: 10,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-10-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-10-sm.png',
        caption: '',
      },
      {
        id: 11,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-11-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-11-sm.png',
        caption: '',
      },
      {
        id: 12,
        src: './TinyPNG-1920x1080/Madeirus/madeirus-12-lg.png',
        thumbnail: './TinyPNG-720x480/Madeirus/madeirus-12-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
    ],
  };
};

export default Madeirus;
