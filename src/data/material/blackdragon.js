const Blackdragon = () => {
  return {
    id: 3,
    materialInfo: {
      materialType: 'Bundle',
      materialName: 'Black Dragon',
      materialCategory: 'Prime',
      isOwnQuarry: false,
    },
    offerData: {
      unitaryValue: '137.5 ',
    },
    slabInfo: {
      ref: '00017540 - 102698',
      qty: '6',
      height: '1.86',
      width: '3.19',
      weight: '3500',
      area: '67.58',
      thickness: '3.0 CM',
      finishing: 'POLISHED FINISH',
    },
    images: [
      {
        id: 31,
        src: './TinyPNG-1920x1080/Black Dragon/black-dragon-02-lg.png',
        thumbnail: './TinyPNG-720x480/Black Dragon/black-dragon-02-sm.png',
        caption: 'Slab Close Up Picture',
      },
      {
        id: 32,
        src: './TinyPNG-1920x1080/Black Dragon/black-dragon-01-lg.png',
        thumbnail: './TinyPNG-720x480/Black Dragon/black-dragon-01-sm.png',
        caption: 'Marvelous mix between wood and granite',
      },
      {
        id: 33,
        src: './TinyPNG-1920x1080/Black Dragon/black-dragon-03-lg.png',
        thumbnail: './TinyPNG-720x480/Black Dragon/black-dragon-03-sm.png',
        caption: 'Tiny Picture',
      },
      {
        id: 34,
        src: './TinyPNG-1920x1080/Black Dragon/black-dragon-04-lg.png',
        thumbnail: './TinyPNG-720x480/Black Dragon/black-dragon-04-sm.png',
        caption: 'Great for use in mutliples surfaces',
      },
      {
        id: 35,
        src: './TinyPNG-1920x1080/Black Dragon/black-dragon-05-lg.png',
        thumbnail: './TinyPNG-720x480/Black Dragon/black-dragon-05-sm.png',
        caption: '',
      },
      {
        id: 36,
        src: './TinyPNG-1920x1080/Black Dragon/black-dragon-06-lg.png',
        thumbnail: './TinyPNG-720x480/Black Dragon/black-dragon-06-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
      {
        id: 37,
        src: './TinyPNG-1920x1080/Black Dragon/black-dragon-07-lg.png',
        thumbnail: './TinyPNG-720x480/Black Dragon/black-dragon-07-sm.png',
        caption: '',
      },
    ],
  };
};

export default Blackdragon;
