const BlueMoon = () => {
  return {
    id: 4,
    materialInfo: {
      materialType: 'Bundle',
      materialName: 'Blue Moon',
      materialCategory: 'Prime',
      isOwnQuarry: false,
    },
    offerData: {
      unitaryValue: '111.5 ',
    },
    slabInfo: {
      ref: '00017540 - 102698',
      qty: '6',
      height: '1.86',
      width: '3.19',
      weight: '3500',
      area: '67.58',
      thickness: '3.0 CM',
      finishing: 'POLISHED FINISH',
    },
    images: [
      {
        id: 41,
        src: './TinyPNG-1920x1080/Blue Moon/blue-moon-04-lg.png',
        thumbnail: './TinyPNG-720x480/Blue Moon/blue-moon-04-sm.png',
        caption: 'Slab Close Up Picture',
      },
      {
        id: 42,
        src: './TinyPNG-1920x1080/Blue Moon/blue-moon-01-lg.png',
        thumbnail: './TinyPNG-720x480/Blue Moon/blue-moon-01-sm.png',
        caption:
          'Versatility is the word for Blue Moon, it can be used in a gourmet area on the balcony.',
      },
      {
        id: 43,
        src: './TinyPNG-1920x1080/Blue Moon/blue-moon-02-lg.png',
        thumbnail: './TinyPNG-720x480/Blue Moon/blue-moon-02-sm.png',
        caption: 'See how the lighting gives other air to the environment.',
      },
      {
        id: 44,
        src: './TinyPNG-1920x1080/Blue Moon/blue-moon-03-lg.png',
        thumbnail: './TinyPNG-720x480/Blue Moon/blue-moon-03-sm.png',
        caption: 'Perfect for use on illuminated walls',
      },
    ],
  };
};

export default BlueMoon;
