const Platinus = () => {
  return {
    id: 8,
    materialInfo: {
      materialType: 'Bundle',
      materialName: 'Platinus',
      materialCategory: 'Standard',
      isOwnQuarry: false,
    },
    offerData: {
      unitaryValue: '88 ',
    },
    slabInfo: {
      ref: '00017540 - 102698',
      qty: '8',
      height: '1.86',
      width: '3.19',
      weight: '3500',
      area: '98.58',
      thickness: '3.0 CM',
      finishing: 'POLISHED FINISH',
    },
    images: [
      {
        id: 1,
        src: './TinyPNG-1920x1080/Platinus/platinus-08-lg.png',
        thumbnail: './TinyPNG-720x480/Platinus/platinus-08-sm.png',
        caption: 'Slab Close Up Picture',
      },
      {
        id: 2,
        src: './TinyPNG-1920x1080/Platinus/platinus-01-lg.png',
        thumbnail: './TinyPNG-720x480/Platinus/platinus-01-sm.png',
        caption: 'Marvelous mix between wood and granite',
      },
      {
        id: 3,
        src: './TinyPNG-1920x1080/Platinus/platinus-03-lg.png',
        thumbnail: './TinyPNG-720x480/Platinus/platinus-03-sm.png',
        caption: 'Tiny Picture',
      },
      {
        id: 4,
        src: './TinyPNG-1920x1080/Platinus/platinus-04-lg.png',
        thumbnail: './TinyPNG-720x480/Platinus/platinus-04-sm.png',
        caption: 'Great for use in mutliples surfaces',
      },
      {
        id: 5,
        src: './TinyPNG-1920x1080/Platinus/platinus-05-lg.png',
        thumbnail: './TinyPNG-720x480/Platinus/platinus-05-sm.png',
        caption: '',
      },
    ],
  };
};

export default Platinus;
