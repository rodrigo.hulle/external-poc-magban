const Alpinus = () => {
  return {
    id: 1,
    materialInfo: {
      materialType: 'Bundle',
      materialName: 'Alpinus',
      materialCategory: 'Prime',
      isOwnQuarry: true,
    },
    offerData: {
      unitaryValue: '137.5 ',
    },
    slabInfo: {
      ref: '00017540 - 102698',
      qty: '6',
      height: '1.86',
      width: '3.19',
      weight: '3500',
      area: '67.58',
      thickness: '3.0 CM',
      finishing: 'POLISHED FINISH',
    },
    images: [
      {
        id: 10,
        src: './TinyPNG-1920x1080/Alpinus/alpinus-02-lg.png',
        thumbnail: './TinyPNG-720x480/Alpinus/alpinus-02-sm.png',
        caption: 'Slab Close Up Picture',
      },
      {
        id: 11,
        src: './TinyPNG-1920x1080/Alpinus/alpinus-01-lg.png',
        thumbnail: './TinyPNG-720x480/Alpinus/alpinus-01-sm.png',
        caption: 'Marvelous mix between wood and granite',
      },
      {
        id: 12,
        src: './TinyPNG-1920x1080/Alpinus/alpinus-03-lg.png',
        thumbnail: './TinyPNG-720x480/Alpinus/alpinus-03-sm.png',
        caption: 'Tiny Picture',
      },
      {
        id: 13,
        src: './TinyPNG-1920x1080/Alpinus/alpinus-04-lg.png',
        thumbnail: './TinyPNG-720x480/Alpinus/alpinus-04-sm.png',
        caption: 'Great for use in mutliples surfaces',
      },
      {
        id: 14,
        src: './TinyPNG-1920x1080/Alpinus/alpinus-05-lg.png',
        thumbnail: './TinyPNG-720x480/Alpinus/alpinus-05-sm.png',
        caption: '',
      },
      {
        id: 15,
        src: './TinyPNG-1920x1080/Alpinus/alpinus-06-lg.png',
        thumbnail: './TinyPNG-720x480/Alpinus/alpinus-06-sm.png',
        caption: 'Magban Exclusive Natural Stone From Own Quarry',
      },
    ],
  };
};

export default Alpinus;
